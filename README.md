## Setup:
1. Code is in ~/Desktop/JavaInterviewQuestion
2. Edit the amount of code wanted for the difficulty of the interview.
3. Make sure you start ~/Applications/h2/bin/h2-1.3.176.jar (should be a shortcut on the desktop)
4. Set the Saved Settings to "Generic H2 (Server)
5. The user/pw should be sa/sa
6. Drop any existing tables (if needed use the create and insert statements below to set up initial data for unit tests)
7. In Java the Dao this should be the connection:

    connectionPool = JdbcConnectionPool.create(
        "jdbc:h2:tcp://localhost/~/test", "sa", "sa");
    JdbcDataSource ds = new JdbcDataSource();
    ds.setURL("jdbc:h2:~/h2db");
    ds.setUser("sa");
    ds.setPassword("sa");
    connection = connectionPool.getConnection();


## Running and Testing
To run use "mvn clean tomcat7:run" this will start the server on localhost:8080/question/

Postman and Advanced Rest Client(ARC) are installed on Chrome if the interviewee wants to test manually.

## Existing Code Assumptions
The table was created with this:

    CREATE TABLE TINY_URLS(TINY_ID LONG PRIMARY KEY AUTO_INCREMENT, TINY_URL VARCHAR(255),
       ORIGINAL_URL VARCHAR(255) NOT NULL, USER VARCHAR(255) NOT NULL, CLICKS LONG);

Initial values that can be inserted:

    INSERT INTO TINY_URLS (TINY_URL,ORIGINAL_URL,USER,CLICKS)
        VALUES ('http://te.com/111','www.google.com','User1', 5);
    
    INSERT INTO TINY_URLS (TINY_URL,ORIGINAL_URL,USER,CLICKS)
        VALUES ('http://te.com/222','www.yahoo.com','User2', 1500);
    
    INSERT INTO TINY_URLS (TINY_URL,ORIGINAL_URL,USER,CLICKS)
        VALUES ('http://te.com/333','www.abcd.com','User1', 42);
    
    INSERT INTO TINY_URLS (TINY_URL,ORIGINAL_URL,USER,CLICKS)
        VALUES ('http://te.com/444','www.penny-arcade.com','User3', 7000);
    
    INSERT INTO TINY_URLS (TINY_URL,ORIGINAL_URL,USER,CLICKS)
        VALUES ('http://te.com/444','www.trueffect.com','routing', 7777);
    
    INSERT INTO TINY_URLS (TINY_URL,ORIGINAL_URL,USER,CLICKS)
        VALUES ('http://te.com/123','www.customUrl.com','CustomUrlUser', 1234);

NOTE: To see any changes in the web-ui from values added from the java app/unit tests you must disconnect from the web interface and 
reconnect to see the new values.  The disconnect button is in the top left of the web ui.  