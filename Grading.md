## Junior Grading:

DAO 
* some of the calls are not using prepared statements see if they notice and change that.  
* Implements getMetrics and delete.
* the call in the lambda function includes a finally block with a empty try catch.  See if they close
* Unit Tests - Implement testCreateCustomUrl, testCreateTinyUrl
 
APP
* See if they make sure business logic exists here and not in the Service of DAO layers
* TinyUrlGenerator - How do they deal with collision resolution?
* Do they figure out a way to deal with full space since the given algo produces a random number from 0-999 inclusive
* Boundary condition thoughts or testing
* Writes more unit tests

REST SERVICE
* Implements missing methods and adds proper Http Verbs (GET, DELETE, POST)
* Adds more unit tests
* Validate / Sanitizes input
* Uses appropriate response codes.
* either accepts encoded urls or takes the url from a passed in object or header param



##Mid-Level Grading

DAO
* Very similar to the Junior grading with these exceptions:
* Prepared statements are not used anywhere.
* There is no finally block hint so no connections are closed in the lambda function
APP
* TinyUrlGenerator - There are 3 implementations of how to get the next url.  They need to pick which one they think will work the best and then deal with the problems it has.  A base 64 encoded function (needs randomness).  A random number generator from 0-9999 inclusive (need to deal with collisions and full space).  A sha-1 hash function of the original URL that returns the first 6 characters (deal with collisions and randomness).
* Use one of the above implementations in createTinyUrl function in the AppImpl
REST
	-Same as Junior level but the redirect has been removed.


##Senior Grading

DAO
* check they implement the getMetrics and delete as well as unit tests missing for other functions.
APP
* Need to create the algorithm that generates the tiny url in TinyUrlGenerator.
* See how they use/move logic out of service/dao layers into app layer
* Boundary condition testing
* Performance
* No collisions and sufficiently random
REST
* Check all their jaxb rest annotations are correct.